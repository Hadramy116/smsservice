package cid.mr.smsnotif.service;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import cid.mr.smsnotif.Model.beans.MessageBean;
import cid.mr.smsnotif.Model.beans.ws.GetMessages;

public class SmsService extends Service {

    private Timer timer;
    private BroadcastReceiver smsSentReceiver;
    private BroadcastReceiver smsDilivredReciever;

    public SmsService() { }

    @Override
    public void onCreate() {
        super.onCreate();
        timer = new Timer();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        Log.d("TIMER_TEST", "run: ");
                        try {
                            ArrayList<MessageBean> msgs = GetMessages.getMessages();
                            for (MessageBean msg : msgs) {
                                if (msg.getStatus() == 0) {
                                    send_sms(msg);
                                }
                            }
                        } catch (Exception e) {
                            Log.v("SEND_ERROR", e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, 1000, 120000);
            }
        }).start();

        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    public void send_sms(final MessageBean msg) {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        PendingIntent sentPI = PendingIntent.getBroadcast(this,
                0, new Intent(SENT), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(this,
                0, new Intent(DELIVERED), 0);

        SmsManager sms = SmsManager.getDefault();
        ArrayList<String> msgContent = sms.divideMessage(msg.getText());
        ArrayList<PendingIntent> sentPendingIntents = new ArrayList<PendingIntent>();
        ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<PendingIntent>();

        for (int i = 0; i < msgContent.size(); i++) {
            sentPendingIntents.add(sentPI);
            deliveredPendingIntents.add(deliveredPI);
        }

        this.smsSentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Thread th = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    msg.setStatus(1);
                                    GetMessages.updateStatus(msg);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        th.start();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getApplicationContext(),
                                "check your sms balance :(", Toast.LENGTH_LONG).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getApplicationContext(),
                                "NO_SERVICE", Toast.LENGTH_LONG).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getApplicationContext(),
                                "NULL_PDU", Toast.LENGTH_LONG).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getApplicationContext(),
                                "RADIO_OFF", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        };
        // ---when the SMS has been delivered---
        this.smsDilivredReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Log.v("SMS", "recu");
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.v("SMS", "RESULT_CANCELED");
                        break;
                }
            }
        };

        this.registerReceiver(smsSentReceiver, new IntentFilter(SENT));
        this.registerReceiver(smsDilivredReciever, new IntentFilter(DELIVERED));

        sms.sendMultipartTextMessage(msg.getPhone(), null, msgContent, sentPendingIntents, deliveredPendingIntents);
        // sms.sendTextMessage(msg.getPhone(), null, msg.getText(), sentPI, deliveredPI);
    }

}
