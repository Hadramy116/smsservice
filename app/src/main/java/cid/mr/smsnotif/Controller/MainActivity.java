package cid.mr.smsnotif.Controller;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import cid.mr.smsnotif.R;
import cid.mr.smsnotif.service.SmsService;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btn_send;
    private Button btn_destroy;
    private Intent intentService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_send = findViewById(R.id.btn_send);
        btn_send.setOnClickListener(this);

        btn_destroy = findViewById(R.id.btn_destroy);
        btn_destroy.setOnClickListener(this);
        btn_destroy.setVisibility(View.INVISIBLE);

        intentService = new Intent(this, SmsService.class);
    }

    @Override
    public void onClick(View v) {
        if (v == btn_send) {
            try {
                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.SEND_SMS},
                            560);
                } else {
                    if (isNetworkAvailable()) {
                        startService(this.intentService);
                        Toast.makeText(getApplicationContext(),"SMS service started",Toast.LENGTH_LONG).show();
                        if (isServiceRunning(SmsService.class)) {
                            btn_send.setTextColor(getResources().getColor(R.color.colorAccent));
                            btn_destroy.setTextColor(getResources().getColor(R.color.colorRed));
                            btn_destroy.setVisibility(View.VISIBLE);
                        }
                    }else{
                        Toast.makeText(this, "Enable network",
                                Toast.LENGTH_SHORT).show();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        } else {
            stopService(this.intentService);
            btn_send.setTextColor(getResources().getColor(R.color.colorBlue));
            btn_destroy.setVisibility(View.INVISIBLE);
        }
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

