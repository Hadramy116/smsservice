package cid.mr.smsnotif.Controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import cid.mr.smsnotif.service.SmsService;

public class BootCompletReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Service sms started", Toast.LENGTH_LONG).show();
        if (intent.getAction() != null) {
            if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED) ||
                    intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
                Intent pushIntent = new Intent(context, SmsService.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    ContextCompat.startForegroundService(context, pushIntent);
                } else {
                    context.startService(pushIntent);
                }
            }
        }
    }
}
