package cid.mr.smsnotif.Model.beans.ws;

import android.util.Log;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkhttpRequest {
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public static Response sendHttpRequest(String url)
            throws Exception {
        Log.v("TAG_URL", url);
        OkHttpClient client = new OkHttpClient();
        Request req = new Request.Builder()
                .header("Content-Type", "application/json")
                .header("Access-Control-Allow-Origin","*")
                .url(url).build();

        return client.newCall(req).execute();
    }

    public static Response updateMsgStatus(String url, String json)
            throws Exception {
        RequestBody formBody = FormBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .put(formBody)
                .build();
        OkHttpClient client = new OkHttpClient();
        return client.newCall(request).execute();
    }
}
