package cid.mr.smsnotif.Model.beans.ws;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;

import cid.mr.smsnotif.Model.beans.MessageBean;
import okhttp3.Response;

public class GetMessages {
    private static final String URL = "https://smsapiservice.herokuapp.com/api/message/";
    private static final Gson gson = new Gson();

    public static ArrayList<MessageBean> getMessages() throws Exception {
        Response response = OkhttpRequest.sendHttpRequest(URL);

        if (response.code() != HttpURLConnection.HTTP_OK) {
            throw new Exception("Réponse du serveur incorrecte : " + response.code());
        } else {
            InputStreamReader inputStreamReader = new InputStreamReader(response.body().byteStream());
            ArrayList<MessageBean> messageBeans = gson.fromJson(inputStreamReader, new TypeToken<ArrayList<MessageBean>>() {
            }.getType());

            inputStreamReader.close();

            return messageBeans;
        }
    }

    public static boolean updateStatus(MessageBean messageBean) throws Exception {
        String json = gson.toJson(messageBean);
        Log.v("json_up",json);
        String url = URL + messageBean.getId() + "/";
        Log.v("UP_URL", url);
        Response response = OkhttpRequest.updateMsgStatus(url, json);
        if (response.code() != HttpURLConnection.HTTP_OK) {
            throw new Exception("update status error " + response.code());
        } else {
            return true;
        }
    }
}