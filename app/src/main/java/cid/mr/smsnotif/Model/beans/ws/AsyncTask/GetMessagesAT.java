package cid.mr.smsnotif.Model.beans.ws.AsyncTask;

import android.os.AsyncTask;

import java.util.ArrayList;

import cid.mr.smsnotif.Model.beans.MessageBean;
import cid.mr.smsnotif.Model.beans.ws.GetMessages;

public class GetMessagesAT extends AsyncTask {

    private ArrayList<MessageBean> msgs;
    private Exception exception;
    private GetMessageATResult getMessageATResult;

    public GetMessagesAT(GetMessageATResult getMessageATResult) {
        this.getMessageATResult = getMessageATResult;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        try {
            msgs = GetMessages.getMessages();
            GetMessages.updateStatus(msgs.get(0));

        } catch (Exception e) {
            exception = e;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        if (getMessageATResult != null) {
            if (exception != null) {
                getMessageATResult.getMessagesErorResul(exception);
            } else {
                getMessageATResult.loadMessages(msgs);
            }
        }
    }

    public interface GetMessageATResult {
        void loadMessages(ArrayList<MessageBean> messageBeans);

        void getMessagesErorResul(Exception exception);
    }
}
