package cid.mr.smsnotif.Model.beans;

public class MessageBean {

    private Integer id;
    private String phone;
    private String name;
    private String text;
    private Integer status;

    public MessageBean() {

    }

    public MessageBean(Integer id, String phone, String name, String text, Integer status) {
        this.id = id;
        this.phone = phone;
        this.name = name;
        this.text = text;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}