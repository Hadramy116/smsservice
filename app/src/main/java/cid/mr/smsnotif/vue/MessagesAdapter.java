package cid.mr.smsnotif.vue;

import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import cid.mr.smsnotif.Model.beans.MessageBean;
import cid.mr.smsnotif.R;


public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MyViewHolder> {

    private ArrayList<MessageBean> data;

    public MessagesAdapter(ArrayList<MessageBean> data) {
        this.data = data;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MessageBean msg = data.get(position);
        holder.display(msg);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView name;
        private final TextView tv_phone;

        private MessageBean msg;

        public MyViewHolder(final View itemView) {
            super(itemView);

            name = ((TextView) itemView.findViewById(R.id.name));
            tv_phone = ((TextView) itemView.findViewById(R.id.tv_phone));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(itemView.getContext())
                            .setTitle("SEND")
                            .setMessage("Content")
                            .show();
                }
            });
        }

        public void display(MessageBean msg) {
            name.setText(msg.getName());
            tv_phone.setText(msg.getPhone());
        }
    }
}
